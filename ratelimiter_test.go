package unirator

import (
	"context"
	"sync"
	"testing"
	"time"
)

func TestConcurrentLimit(t *testing.T) {
	t.Run("can acquire id", func(t *testing.T) {
		rl, err := NewConcurrentLimiter(nil)
		if err != nil {
			t.Errorf("failed to get limiter: %v", err)
		}

		chRec := make(chan *RateLimitID)
		chErr := make(chan error)
		go func() {
			id, err := rl.Acquire()
			if err != nil {
				chErr <- err
			} else {
				chRec <- id
			}
		}()

		select {
		case err := <-chErr:
			t.Errorf("failed to acquire: %v", err)
		case got := <-chRec:
			if got.ID == "" {
				t.Errorf("did not acquire id")
			}
		case <-time.After(1 * time.Second):
			t.Errorf("timed out waiting for acquire")
		}

	})

	t.Run("can limit to 3 jobs", func(t *testing.T) {
		ctx, cancel := context.WithCancel(context.Background())
		// TODO need to pass in own struct to handle waiting better.
		rl, _ := NewConcurrentLimiterWithCtx(ctx, &Config{ratelimit: 3})
		var wg sync.WaitGroup

		for i := 0; i < 5; i++ {
			wg.Add(1)
			go func() {
				rl.Acquire()
				wg.Done()
			}()
		}

		var activeIdCount int
		var pendingIdCount int

		for {
			activeIdCount = rl.GetActiveIdCount()
			pendingIdCount = rl.GetPendingIdCount()
			if (activeIdCount + pendingIdCount) >= 5 {
				break
			}
			time.Sleep(1 * time.Millisecond)
		}

		if pendingIdCount != 2 {
			t.Errorf("expected 2 ids waiting, got: %v", pendingIdCount)
		}

		if activeIdCount != 3 {
			t.Errorf("expected 3 ids got, got: %v", activeIdCount)
		}

		cancel()
		wg.Wait()
	})

	t.Run("can cancel concurrent limiter", func(t *testing.T) {
		ctx, cancel := context.WithCancel(context.Background())
		cErr := make(chan error)
		// TODO need to pass in own struct to handle waiting better.
		rl, _ := NewConcurrentLimiterWithCtx(ctx, nil)

		go func() {
			_, err := rl.Acquire()
			cErr <- err
		}()
		cancel()

		select {
		case err := <-cErr:
			t.Logf("got err: %v", err)
		case <-time.After(3 * time.Second):
			t.Errorf("timed out waiting for cancel")
		}
	})

	t.Run("can release id for jobs", func(t *testing.T) {
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()
		cId := make(chan *RateLimitID)
		// set limit to 1 so we can check second Acquire
		rl, _ := NewConcurrentLimiterWithCtx(ctx, &Config{ratelimit: 1})

		// get id and send to channel
		for i := 0; i < 2; i++ {
			go func() {
				id, _ := rl.Acquire()
				cId <- id
			}()
		}

		// wait for the id from the first channel then release it
		select {
		case id := <-cId:
			rl.Release(id)
		case <-time.After(1 * time.Second):
			t.Fatalf("timed out waiting for id")
		}

		//wait for id from second channel which proves we can release the first
		select {
		case <-cId:
			break
		case <-time.After(1 * time.Second):
			t.Fatalf("timed out waiting for id")
		}

		activeIdCount := rl.GetActiveIdCount()
		pendingIdCount := rl.GetPendingIdCount()

		if pendingIdCount != 0 {
			t.Errorf("expected 0 ids pending, got: %v", pendingIdCount)
		}

		if activeIdCount != 1 {
			t.Errorf("expected 1 ids active, got: %v", activeIdCount)
		}

	})
	// 	// Note: Can add user or other info to context.Value for logging specific requests.

}

func TestRateLimiter(t *testing.T) {
	t.Run("can acquire id", func(t *testing.T) {
		rl := NewRateLimiter(nil)

		chRec := make(chan *RateLimitID)
		chErr := make(chan error)
		go func() {
			rl.getNewID()
			id, err := rl.Acquire()
			if err != nil {
				chErr <- err
			} else {
				chRec <- id
			}
		}()

		select {
		case err := <-chErr:
			t.Errorf("failed to acquire: %v", err)
		case got := <-chRec:
			if got.ID == "" {
				t.Errorf("did not acquire id")
			}
		case <-time.After(1 * time.Second):
			t.Errorf("timed out waiting for acquire")
		}
	})

	t.Run("can cancel limiter", func(t *testing.T) {
		cErr := make(chan error)

		ctx, cancel := context.WithCancel(context.Background())
		rl := NewRateLimiterWithCtx(ctx, nil)

		go func() {
			_, err := rl.Acquire()
			cErr <- err
		}()

		cancel()
		select {
		case <-cErr:
			break
		case <-time.After(1 * time.Second):
			t.Errorf("timed out waiting for cancel")
		}
	})

	t.Run("can timeout limiter", func(t *testing.T) {
		cErr := make(chan error)
		cId := make(chan *RateLimitID)

		rl := NewRateLimiter(&Config{timeout: 10 * time.Millisecond})

		go func() {
			rl.getNewID() //get the id with the timeout
			id, err := rl.Acquire()
			if id != nil {
				cId <- id
			} else {
				cErr <- err
			}
		}()

		var id *RateLimitID
		// wait for error or id
		select {
		case err := <-cErr:
			t.Errorf("expected id got err: %v", err)
		case id = <-cId:
		case <-time.After(1000 * time.Millisecond):
			t.Errorf("timed out waiting for cancel")
		}

		//got id now wait for timeout
		select {
		case <-id.ctx.Done():
		case <-time.After(1000 * time.Millisecond):
			t.Errorf("timed out waiting for cancel")
		}
	})
}
