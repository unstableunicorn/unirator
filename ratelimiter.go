package unirator

import (
	"context"
	"fmt"
	"sync"
	"sync/atomic"
	"time"

	"github.com/google/uuid"
)

// RateLimit exposes an Acquire() method for obtaining a Rate Limit ID
type RateLimit interface {
	Acquire() (*RateLimitID, error)
	Release(*RateLimitID)
	GetActiveIdCount() int
	GetPendingIdCount() int
}

// RateLimitID contains the details of the Rate Limit ID
type RateLimitID struct {
	ID      string
	ctx     context.Context
	release context.CancelFunc
}

// RateLimiter implements the RateLimit Interface
type RateLimiter struct {
	chOut      chan *RateLimitID
	chIn       chan struct{}
	chRelease  chan *RateLimitID
	idList     map[string]*RateLimitID
	idsPending int64
	limit      int
	timeout    time.Duration
	ctx        context.Context
	mu         sync.Mutex
}

type Config struct {
	ratelimit int
	timeout   time.Duration
}

// NewRateLimiter creates a new Rate Limiter
func NewRateLimiter(config *Config) *RateLimiter {
	return NewRateLimiterWithCtx(context.Background(), config)
}

// NewRateLimiter creates a new Rate Limiter with Context
func NewRateLimiterWithCtx(ctx context.Context, config *Config) *RateLimiter {
	if config == nil {
		config = &Config{}
	}
	rl := &RateLimiter{
		chOut:      make(chan *RateLimitID),
		chIn:       make(chan struct{}),
		chRelease:  make(chan *RateLimitID),
		idList:     make(map[string]*RateLimitID),
		idsPending: 0,
		limit:      config.ratelimit,
		timeout:    config.timeout,
		ctx:        ctx,
	}
	return rl
}

// Acquire is called to get a new ID for the Rate Limit
func (r *RateLimiter) Acquire() (*RateLimitID, error) {
	go func() {
		r.chIn <- struct{}{}
	}()

	// Await the id
	select {
	case id := <-r.chOut:
		return id, nil
	case <-r.ctx.Done():
		return nil, fmt.Errorf("acquire: %v", r.ctx.Err())
	}
}

// Release is called to remove a token
// TODO: Added but now simple so can remove?
func (r *RateLimiter) Release(id *RateLimitID) {
	go func() {
		r.chRelease <- id
	}()
}

// GetActiveIdCount is called to return the count of in use Id's
func (r *RateLimiter) GetActiveIdCount() int {
	r.mu.Lock()
	defer r.mu.Unlock()
	idCount := len(r.idList)
	return idCount
}

// GetPendingIdCount is called to return the number of id's pending
func (r *RateLimiter) GetPendingIdCount() int {
	return int(atomic.LoadInt64(&r.idsPending))
}

// Called when a new ID is required
func (r *RateLimiter) getNewID() {
	// if timeout in config make sure to get one that times out
	if r.timeout != 0 {
		r.getNewIDWithTimeout()
		return
	}

	id := &RateLimitID{
		ID:  uuid.New().String(),
		ctx: r.ctx,
	}
	r.addIdToList(id)

	go func() {
		r.chOut <- id
	}()
}

func (r *RateLimiter) addIdToList(id *RateLimitID) {
	r.mu.Lock()
	defer r.mu.Unlock()
	r.idList[id.ID] = id
}

// Called when a new time limited ID is required
func (r *RateLimiter) getNewIDWithTimeout() {

	ctxTO, cancel := context.WithTimeout(r.ctx, r.timeout)

	id := &RateLimitID{
		ID:      uuid.New().String(),
		ctx:     ctxTO,
		release: cancel,
	}
	r.addIdToList(id)

	// send out the id and prepare to release id if cancelled
	go func() {
		r.chOut <- id
		<-ctxTO.Done()
		// context is Done, id needs to be manually released
		r.releaseID(id)
	}()
}

func (r *RateLimiter) releaseID(id *RateLimitID) {
	// remove from list
	delete(r.idList, id.ID)

	// decrement counter if counter is > 0, must be atomic to avoid race
	if atomic.LoadInt64(&r.idsPending) > 0 {
		atomic.AddInt64(&r.idsPending, -1)
		go r.getNewID()
	}
}
