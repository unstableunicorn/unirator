package unirator

import (
	"context"
	"sync/atomic"
)

// NewConcurrentLimiter returns a new limiter that can limites concurrent tasks
func NewConcurrentLimiter(config *Config) (RateLimit, error) {
	return NewConcurrentLimiterWithCtx(context.Background(), config)
}

// NewConcurrentLimiterWithCtx returns a new limiter that can limites concurrent tasks and pass a ctx for cancelling the wait for a newID
func NewConcurrentLimiterWithCtx(ctx context.Context, config *Config) (RateLimit, error) {

	r := NewRateLimiterWithCtx(ctx, config)
	go func() {
	cloop:
		for {
			select {
			case <-r.chIn:
				// limit amount of id's in use
				if (r.limit > 0) && (len(r.idList) >= r.limit) {
					atomic.AddInt64(&r.idsPending, 1)
				} else {
					r.getNewID()
				}
			case id := <-r.chRelease:
				//Release() was called with id so we can remove it
				r.releaseID(id)
			case <-ctx.Done():
				break cloop //Note: could return instead here but wanted to test this
			}
		}
	}()

	return r, nil
}
